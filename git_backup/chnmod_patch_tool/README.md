###1. 这些选项是做什么的

![ScreenShot](http://i.imgur.com/pXykfeh.png)

###COMPARE

用于生成比较时的.old档案

###DEBUG

用于检验文本，扫描内容时，如果发现错误，你就可以在记录档（mods\logs\）找当日的文件，最后一行，看刚才扫描到哪，那下一行就是有错误的那一行

若有需要，可以透过修改mods\saves\CHNMOD_PATCH.txt 来开启这功能

###DETECT

如果文档有缺漏，导致某些没被定义，那开启后，就会将没被定义过的前面加上关键ID

###OUTPUT

将文本导出成.json 随后就可透过https://json-csv.com/ 转成csv或其他格式

.

.

###2. 如何将官方的strings文件与现有的做比较

####进入MOD选项开启[COMPARE]，随后重新开启游戏

![ScreenShot](http://i.imgur.com/oor1T1d.jpg)

![ScreenShot](http://i.imgur.com/6I8TTdw.jpg)

就会产生所需要的.old档案

![ScreenShot](http://i.imgur.com/OTQhQgb.png)

####安装［[python](https://coding.net/u/Dr_Newbie/p/chnmod_patch_tool/git/blob/master/python-2.7.11.7z)］

####下载［[strings_extractor.py](https://coding.net/u/Dr_Newbie/p/chnmod_patch_tool/git/blob/master/strings_extractor.py)］、［[strings_extractor.bat](https://coding.net/u/Dr_Newbie/p/chnmod_patch_tool/git/blob/master/strings_extractor.bat)］

####下载［[Bundle Modder](http://downloads.lastbullet.net/197)］，并用他拆出所需要的.strings文件

f8b759c0f7048df4

![ScreenShot](https://coding.net/u/Dr_Newbie/p/payday2_qna/git/raw/master/PIC/%25E6%258B%2586%25E5%2587%25BA%25E6%2589%2580%25E9%259C%2580%25E8%25A6%2581%25E7%259A%2584.strings%25E6%2596%2587%25E4%25BB%25B6.png)

####将［strings_extractor.py］、［strings_extractor.bat］、［.old档案］、［.strings文件］放置于同个目录底下后执行［strings_extractor.bat］就可完成，如果比较中有发现新的字词，就可在新的［---NEW---］里面找到
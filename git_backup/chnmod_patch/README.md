# ''chnmod_patch'' 汉化MOD (补充)

##你非常需要知道的事情

就是我没兴趣帮你们解决问题，用不了我也懒得管，这东西免费开源，没欠你什么。

我会去更新和除错，但是超过我的热情和能力，就不会去做。

##喊什么MOD用不了的

看这视频： http://www.bilibili.com/video/av4868324/

正常工作。

我电脑也没改什么，就是微软那下载WIN10 然后激活，没安装什么特别补充，没搞什么特别修改等等等。

## 常见问题

###0. 用得好好的却在某一天突然出现问题？

试试重新下载和 http://tieba.baidu.com/p/4558405498

###1. 更新没完怎么办?
如果你确定是真的不停更新或是单纯的下载不了（空间被阻挡之类的），

你可以手动新增或下载 [mods/chnmod_patch/loc/version.txt(点)](loc/version.txt)

###2. 怎么下载?

####[下载MOD(点)](http://t.im/131np)

####[下载BLT 主档(点)](https://paydaymods.com/download/) [(Download 2)(点)](http://t.cn/RqjzXIX)

####[下载字库(点)](http://t.im/pd2chnfonts)
 
###3. 怎么安装?
BLT的基本安装教学请参考这：http://tieba.baidu.com/p/4005085597

上面安装完后，删除其他之前的汉化，下载这[汉化(点)](http://t.im/131np)

下载后把里面的mods \ assets 拿去复盖你的PAYDAY 2主文件夹

![ScreenShot(点)](http://i.imgur.com/sdAIsAZ.png)

安装失败或是不会用的，你也不必来问我，因为流程就是如此（下载，解压缩，放到指定位置，需要安装[VC(点)](http://tieba.baidu.com/photo/p?kw=%E6%94%B6%E8%8E%B7%E6%97%A52&flux=1&tid=4005085597&pic_id=da713ff431adcbefee62d26daaaf2edda2cc9f5e&pn=1&fp=2&see_lz=1)），你硬是弄出问题，那流程还是如此，你电脑就是不接受，那去问你的电脑为什么不照办。你自己也可以反问你自己，「我除了说『没有成功』这四个字之外，能不能说出更多事情？」，这是因为，是，你跟我说你失败了，然后呢？我这里都正常，你要我怎么检测你的问题？用猜的吗？「啊，可能是因为没有五星连珠，气场不对，导致你安装失败」，这样吗？当然不可能。



###4. 如何确定自己能不能更新?
你可以手动删除［mods/chnmod_patch/loc/version.txt］或是用按钮执行，之后重新进入游戏时，就会替出更新讯息，一路回车刷新到底

http://i.imgur.com/CWKE8Eh.jpg

http://i.imgur.com/ocULBW1.jpg

http://i.imgur.com/lfVmtXm.jpg

http://i.imgur.com/okLuAiP.jpg

http://i.imgur.com/YwEaDix.jpg

###5. 「Mythings」是做什么的?
一个放置于主页的互动功能，当你按下去时，就会执行我放在[my_things(点)](mods/chnmod_patch/loc/my_things) 里的代码

你若不需要，可以在MOD选项内关闭它

![ScreenShot(点)](http://i.imgur.com/0wOztlu.png)

###6. 我有其他问题想问
请到，http://tieba.baidu.com/p/4157500572

记得要标注是chnmod_patch
Hooks:Add("LocalizationManagerPostInit", "LocalizationManagerPostInit_HoxHud",
          function(loc)
             if not tweak_data.hoxhud then
                return
             end
             local texts = {}
             local hoxhud_locfile = io.open("mods/modschn/HoxHudLocalisation.json")
             if hoxhud_locfile then
                local hoxhud_loc_string = hoxhud_locfile:read("*a")
                local my_hoxhud_loc_strings = json.decode(hoxhud_loc_string)
                io.close(hoxhud_locfile)
                for k, v in pairs(my_hoxhud_loc_strings) do
                   if type(v) == "string" then
                      texts[k] = v
                   end
                end
                if tweak_data.hoxhud.local_strings then
                   rawset(tweak_data.hoxhud, "local_strings", my_hoxhud_loc_strings)
                   rawset(tweak_data.hoxhud, "assault_phase_text",
                          my_hoxhud_loc_strings.hoxhud_assault_phase_text)
                   rawset(tweak_data.hoxhud, "assault_spawn_amount_text",
                          my_hoxhud_loc_strings.hoxhud_spawn_amount_text)
                   rawset(tweak_data.hoxhud, "phase_map",
                          my_hoxhud_loc_strings.hoxhud_assault_phase_map)
                   rawset(tweak_data.hoxhud, "assault_time_left_text",
                          my_hoxhud_loc_strings.hoxhud_assault_time_left_text)
                   rawset(tweak_data.hoxhud, "timer_name_map",
                          my_hoxhud_loc_strings.hoxhud_timer_name_map)
                   for k, v in pairs(my_hoxhud_loc_strings.hoxhud_timer_name_map) do
                      rawset(tweak_data.hoxhud, k .. "_name", v)
                   end
                end
             end
             loc:add_localized_strings(texts)
          end
)

Hooks:Add("LocalizationManagerPostInit", "LocalizationManagerPostInit_Restortion_CHN",
          function( loc )
             loc:load_localization_file("mods/modschn/HudRestoration.txt")
          end
)

ModsChnPocoLocalizationData = ModsChnPocoLocalizationData or nil
PocoHudReHook = PocoHudReHook or nil

local inGame = CopDamage ~= nil
local upgradeSubst = { Bulletstorm = "子弹风暴",
                       SwanSong = "天鹅之歌",
                       Overkill = "杀戮+",
                       Underdog = "落水狗+",
                       CombatMedic = "战地医生+",
                       Painkiller = "止痛药-",
                       FirstAid = "急救包-",
                       LifeLeech = "吸血",
                       CloseCombat = "近战",
                       stamina = "精疲力尽" }

local popupSubst = {}
popupSubst["Hands-Up"] = "举起双手"
popupSubst["Intimidated"] = "屈服投降"

if not ModsChnPocoLocalizationData then
   log("Loading PocoHud localization file")
   local pocohud_locfile = io.open("mods/modschn/PocoHudLocalization.json")
   if pocohud_locfile then
      local pocohud_loc_string = pocohud_locfile:read("*a")
      ModsChnPocoLocalizationData = json.decode(pocohud_loc_string)
   end
   pocohud_locfile:close()
else
   if ModsChnPocoLocalizationData and PocoHud3Class and PocoHud3Class.L then

      for k, v in pairs(ModsChnPocoLocalizationData) do
          PocoHud3Class.L.data[k] = v
          PocoHud3Class.L._data[k] = v
      end

--      PocoHud3Class.L.data = ModsChnPocoLocalizationData
--      PocoHud3Class.L._data = ModsChnPocoLocalizationData

      if not PocoHudReHook and PocoHud3 then
         local buff_func = PocoHud3.Buff
         PocoHud3.Buff = function(this, data)
            if data and type(data) == "table" and data.text and data.text ~= "" then
                if upgradeSubst[data.key] then
                   data.text = upgradeSubst[data.key]
                else
                   log("Upgrade unknown key " .. data.key)
                end
            end
            buff_func(this, data)
         end

         local pop_func = PocoHud3.Popup
         PocoHud3.Popup = function(this, data)
            if data and data.text and data.text[1] and data.text[1][1] then
               local k = data.text[1][1]
               local l = popupSubst[k]
               if l then
                  data.text[1][1] = l
               end
            end
            pop_func(this, data)
         end
         PocoHudReHook = true
      end
   end
end
